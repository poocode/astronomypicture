
# Prueba ténica iOS - SoyYo
La prueba se basa en la realización de una App que contenga las siguientes características:
- Que liste **"APOD - Astronomy Picture of the Day"** de la API pública de la NASA de los últimos 8 días, es decir, si hoy estamos a 8 de enero, vamos a poder visualizar los ítems desde el 1 de enero. Url poder interactuar con el API https://api.nasa.gov/.

- Una vez listados los ítems, se podrá seleccionar cualquiera de ellos e ir a una pantalla que contenga información más detallada.

- Se podrá realizar una búsqueda de la imagen del día, seleccionando determinada fecha **(Opcional).**

## ¿Qué vamos a tener en cuenta?

> **El diseño es libre, así que tendremos en cuenta la adaptación de las pantallas a diferentes dispositivos.**

> **Arquitectura.**

> **Animaciones.**

> **Código limpio y buenas prácticas de programación.**

## ¿Qué debe contener la entrega?

**1.** Video del funcionamiento de la App.
**2.** Código fuente en un archivo zip o subido en repositorio (proveer url).
**3.** Si se usaron dependencias, explicar el porqué de su uso.

## ¡Nota!

>**Lo mencionado a continuación fue empleado en este proyecto, se podrá validar en la codificación y estructura de este**

## Arquitectura utilizada VIPER

VIPER es un acrónimo de **View**, **Interactor**, **Presenter**, **Entity** y **Router**. Esta arquitectura está basada en Principios de Responsabilidad Unica, que guía hacia una arquitectura más limpia, para tener una estructura mejor [VIPER]([https://apiumhub.com/es/tech-blog-barcelona/arquitectura-viper/](https://apiumhub.com/es/tech-blog-barcelona/arquitectura-viper/)). 

```mermaid

graph TD
A(VIEW)  --- B(PRESENTER)
B --- C(WIREFRAME)
B --- D(ITERACTOR)
D --- E(REMOTE DATA MANAGER)
D --- G(LOCAL DATA MANAGER)
D --- F(ENTITY)
```

## Diagrama  UML 

La finalidad de los diagramas es presentar diversas perspectivas de un sistema, a las cuales se les conoce como modelo.

```mermaid

sequenceDiagram
VIEW ->> PRESENTER: Fecha
PRESENTER-->>ITERACTOR: Solicitud

ITERACTOR-->> REMOTE: Llamado al servicio Rest
REMOTE-->> ITERACTOR: Respuesta ok
ITERACTOR-->> PRESENTER: Serializacion
PRESENTER ->> VIEW: Llenar tabla con los datos
REMOTE--x ITERACTOR: Respuesta Error
ITERACTOR--x PRESENTER: Error servicio
PRESENTER --x VIEW: Mostrar dialogo de error
```

## Control de versiones GITFLOW 

El flujo de trabajo de Gitflow es un diseño de flujo de trabajo de Git que fue publicado por primera vez y popularizado por [Vincent Driessen en nvie](http://nvie.com/posts/a-successful-git-branching-model/). El flujo de trabajo de Gitflow define un modelo estricto de ramificación diseñado alrededor de la publicación del proyecto. Proporciona un marco sólido para gestionar proyectos más grandes.

![enter image description here](https://3.bp.blogspot.com/-_tuoCwk7YWw/WkHM4Dep2ZI/AAAAAAAACA4/dxbaMtFhHrUjL1vIkL-Ujlq5VFTpYYAqACLcBGAs/s1600/04%2B%25281%2529.png)


## Clean Code: código limpio

Clean Code, o Código Limpio, es una filosofía de desarrollo de software que consiste en aplicar **técnicas simples que facilitan la escritura y lectura de un código**, volviéndolo más fácil de entender.


## Librerías utilizadas

- **`'Alamofire', '~>  4.8.0'.`**
	> Esta es una librería especializada para consumir servicios **REST** y **SOAP** de una forma asincrónica y fácil de implementar

- **`'AlamofireObjectMapper', '~> 5.2'.`**
	> Esta librería es la encargada de serializar los datos recibidos de una forma **muy sencilla**

- **'`JGProgressHUD'.`**
	> Con esta librería mostramos por pantalla un dialogo de cargando información, mientras el servicio realiza la petición al API

- **`'SDWebImage', '~> 4.0'.`**
	> Esta librería es la encargada de mostrar las imágenes que vienen en el servicio en formato URL y también las guarda en el cache del dispositivo para que la próxima ves carguen más rápido

- **`'XCDYouTubeKit'.`**
	> Esta librería nos ayuda a codificar los videos de **Youtube** que vienen en el servicio para mostrarlos por pantalla

- **`'RevealingSplashView'.`**
	> Se implemento esta librería para realizar la animación del Splash de la aplicación el resto de implementación se realizo por código

- **`'DatePickerDialog'.`**
	> Esta librería nos ayuda a mostrar un dialogo con un `dataPicker` donde seleccionaremos una fecha en concreto, que a su ves realice una petición al servicio


## Pruebas unitarias y TDD 

Se realizaron pruebas unitarias al servicio y las funciones de utilidades. Se puede realizar más pruebas en la parte UI, pero por la entrega y tiempo se hacen estas a modo de precedente de su implementación 

![enter image description here](https://appdomidomi.com/Captura.png)