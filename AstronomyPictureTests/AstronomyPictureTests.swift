//
//  AstronomyPictureTests.swift
//  AstronomyPictureTests
//
//  Created by German Garcia on 22/08/20.
//  Copyright © 2020 German Garcia. All rights reserved.
//

import XCTest
@testable import AstronomyPicture

class AstronomyPictureTests: XCTestCase {
    
    var remoteDatamanager: HomeRemoteDataManagerInputProtocol?
    var myDatesViewController : HomeView?
    
    override func setUp() {
        super.setUp()
        
        let storyboard1 = UIStoryboard(name: "HomeView", bundle: Bundle(for: HomeView.self))
        myDatesViewController = storyboard1.instantiateViewController(withIdentifier: "homeView") as? HomeView
        let _ = myDatesViewController!.view
        
    }
    
    override func tearDown() {
        super.tearDown()
    }

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testFormatDate() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        var dateComponents = DateComponents()
        dateComponents.year = 2020
        dateComponents.month = 10
        dateComponents.day = 11
        let userCalendar = Calendar.current
        let someDateTime = userCalendar.date(from: dateComponents)
        let dato = CurrentDateToString.currentDateToString(format: "yyy-MM-dd", date: someDateTime!)
        XCTAssertEqual("2020-10-11", dato)
    }
    
    
    func testMessages() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        XCTAssertEqual("52xPGyIGysFelwg8Bx3LeMkhpojFfAriH7ibsIhB", Constants.KEYSERVICE.KEY)
        XCTAssertEqual("https://api.nasa.gov/", Constants.PARAMETERS.ENDPOINT)
    }
    
    func testValidateViewServices() throws {
        XCTAssertNotNil(myDatesViewController?.viewDidLoad())
    }

}
