//
//  CurrentDateToString.swift
//  AstronomyPicture
//
//  Created by German Garcia on 21/08/20.
//  Copyright © 2020 German Garcia. All rights reserved.
//

import Foundation

class CurrentDateToString {
    
    class public func currentDateToString(format: String, date: Date) -> String {
        let formatter = DateFormatter()
        let locale = Locale.current.identifier
        formatter.locale = Locale(identifier: locale)
        formatter.dateFormat = format
        let stringCurrentDate = formatter.string(from: date)
        return stringCurrentDate
    }
    
}
