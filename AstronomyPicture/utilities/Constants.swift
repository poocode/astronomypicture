//
//  Constants.swift
//  AstronomyPicture
//
//  Created by German Garcia on 20/08/20.
//  Copyright © 2020 German Garcia. All rights reserved.
//

import Foundation

struct Constants {
    
    struct PARAMETERS {
        static let ENDPOINT = "https://api.nasa.gov/"
    }
    
    struct KEYSERVICE {
        static let KEY = "52xPGyIGysFelwg8Bx3LeMkhpojFfAriH7ibsIhB"
    }
    
    struct MESSAGES {
        static let BUTTONTITLEACCEPT = "OK"
        static let TITLEALERVIEW = "Ups!"
        static let LOADING = "loading"
        static let SELECTDAY = "Select a day"
        static let DONE = "Done"
        static let CANCEL = "Cancel"
        
        
    }
    
}
