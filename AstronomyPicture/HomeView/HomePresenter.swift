//
//  HomePresenter.swift
//  AstronomyPicture
//
//  Created by German Garcia on 20/08/20.
//  Copyright © 2020 German Garcia. All rights reserved.
//

import Foundation

class HomePresenter  {
    
    // MARK: Properties
    weak var view: HomeViewProtocol?
    var interactor: HomeInteractorInputProtocol?
    var wireFrame: HomeWireFrameProtocol?
    
}

extension HomePresenter: HomePresenterProtocol {

    // TODO: implement presenter methods
    func viewDidLoad(startDate: String, dateBefore: String) {
        view?.showSpinner()
        interactor?.getAstronomyPictureDay(startDate: startDate, dateBefore: dateBefore)
    }
    
    func showViewDetail(with astronomyDTO: AstronomyDTO) {
        wireFrame?.presenterNewViewDetail(from: view!, astronomyDTO: astronomyDTO)
    }
}

extension HomePresenter: HomeInteractorOutputProtocol {
    
    // TODO: implement interactor output methods
    func interatorPushAstronomyPresenter(with listAstronomyDTO: [AstronomyDTO]) {
        view?.dismissSpinner()
        view?.viewAstronomy(with: listAstronomyDTO)
    }
    
    func interatorPushError(with error: String) {
        view?.dismissSpinner()
        view?.viewError(with: error)
    }
    
}
