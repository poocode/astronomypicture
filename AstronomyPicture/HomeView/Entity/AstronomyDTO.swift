//
//  AstronomyDTO.swift
//  AstronomyPicture
//
//  Created by German Garcia on 20/08/20.
//  Copyright © 2020 German Garcia. All rights reserved.
//

import Foundation
import ObjectMapper

class AstronomyDTO: NSObject, Mappable {
    
    var copyright: String?
    var date: String?
    var explanation: String?
    var hdurl: String?
    var media_type: String?
    var service_version: String?
    var title: String?
    var url: String?
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        copyright <- map["copyright"]
        date <- map["date"]
        explanation <- map["explanation"]
        hdurl <- map["hdurl"]
        media_type <- map["media_type"]
        service_version <- map["service_version"]
        title <- map["title"]
        url <- map["url"]
    }
    
}
