//
//  HomeProtocols.swift
//  AstronomyPicture
//
//  Created by German Garcia on 20/08/20.
//  Copyright © 2020 German Garcia. All rights reserved.
//

import Foundation
import UIKit

protocol HomeViewProtocol: class {
    // PRESENTER -> VIEW
    var presenter: HomePresenterProtocol? { get set }
    
    func viewAstronomy(with listAstronomyDTO: [AstronomyDTO])
    func viewError(with error: String)
    func showSpinner()
    func dismissSpinner()
}

protocol HomeWireFrameProtocol: class {
    // PRESENTER -> WIREFRAME
    static func createHomeModule() -> UIViewController
    func presenterNewViewDetail(from view: HomeViewProtocol, astronomyDTO: AstronomyDTO)
}

protocol HomePresenterProtocol: class {
    // VIEW -> PRESENTER
    var view: HomeViewProtocol? { get set }
    var interactor: HomeInteractorInputProtocol? { get set }
    var wireFrame: HomeWireFrameProtocol? { get set }
    
    func viewDidLoad(startDate: String, dateBefore: String)
    func showViewDetail(with astronomyDTO: AstronomyDTO)
    
}

protocol HomeInteractorOutputProtocol: class {
    // INTERACTOR -> PRESENTER
    func interatorPushAstronomyPresenter(with listAstronomyDTO: [AstronomyDTO])
    func interatorPushError(with error: String)
}

protocol HomeInteractorInputProtocol: class {
    // PRESENTER -> INTERACTOR
    var presenter: HomeInteractorOutputProtocol? { get set }
    var localDatamanager: HomeLocalDataManagerInputProtocol? { get set }
    var remoteDatamanager: HomeRemoteDataManagerInputProtocol? { get set }
    
    func getAstronomyPictureDay(startDate: String, dateBefore: String)
}

protocol HomeDataManagerInputProtocol: class {
    // INTERACTOR -> DATAMANAGER
}

protocol HomeRemoteDataManagerInputProtocol: class {
    // INTERACTOR -> REMOTEDATAMANAGER
    var remoteRequestHandler: HomeRemoteDataManagerOutputProtocol? { get set }
    func remoteGetAstronomyPictureDay(startDate: String, dateBefore: String)
}

protocol HomeRemoteDataManagerOutputProtocol: class {
    // REMOTEDATAMANAGER -> INTERACTOR
    func callBackResponseAstronomyPictureDay(with ListAstronomyDTO: [AstronomyDTO])
    func callBackResponseError(with error: String)
}

protocol HomeLocalDataManagerInputProtocol: class {
    // INTERACTOR -> LOCALDATAMANAGER
}
