//
//  HomeView.swift
//  AstronomyPicture
//
//  Created by German Garcia on 20/08/20.
//  Copyright © 2020 German Garcia. All rights reserved.
//

import Foundation
import UIKit
import JGProgressHUD
import DatePickerDialog

class HomeView: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblAstronomy: UITableView!
    
    var listAstronomyDTO = [AstronomyDTO]()
    let hud = JGProgressHUD(style: .dark)
    
    let datePicker = DatePickerDialog(
        textColor: UIColor(named: "lines")!,
        buttonColor: .black,
        showCancelButton: true
    )
    
    // MARK: Properties
    var presenter: HomePresenterProtocol?

    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblAstronomy.register(UINib.init(nibName: "RefreshCell", bundle: nil), forCellReuseIdentifier: "RefreshCell")
        
        Timer.scheduledTimer(withTimeInterval: 3, repeats: false) { (timer) in
          // sending notif here
          NotificationCenter.default.post(name: heartAttackNotificationName, object: nil)
        }
        
        self.recoverData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if listAstronomyDTO.count > 0 {
            return listAstronomyDTO.count
        } else {
            return 10
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if listAstronomyDTO.count > 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RefreshCell", for: indexPath) as! RefreshCell
            cell.astronomyDTO = listAstronomyDTO[indexPath.row]
            return cell
        } else {
           let cell = tableView.dequeueReusableCell(withIdentifier: "RefreshCell", for: indexPath) as! RefreshCell
           return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.showViewDetail(with: listAstronomyDTO[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    @IBAction func btnSearchImage(_ sender: Any) {
       let currentDate = Date()
       var dateComponents = DateComponents()
       dateComponents.month = -5
       let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)

       datePicker.show(Constants.MESSAGES.SELECTDAY, doneButtonTitle: Constants.MESSAGES.DONE, cancelButtonTitle: Constants.MESSAGES.CANCEL,
                       minimumDate: threeMonthAgo,
                       maximumDate: currentDate,
                       datePickerMode: .date) { (date) in
           if let dt = date {
                let dateSelect = CurrentDateToString.currentDateToString(format: "yyy-MM-dd", date: dt)
                self.presenter?.viewDidLoad(startDate: dateSelect, dateBefore: dateSelect)
           }
       }
    }
    @IBAction func btnRefreshes(_ sender: Any) {
        self.recoverData()
    }
    
    func recoverData() {
        let startDate = CurrentDateToString.currentDateToString(format: "yyy-MM-dd", date: Date())
        let dateBefore =  CurrentDateToString.currentDateToString(format: "yyy-MM-dd", date: Date().dateBeforeOrAfterFromToday(numberOfDays : -8))
        self.presenter?.viewDidLoad(startDate: startDate, dateBefore: dateBefore)
    }
    
}

extension HomeView: HomeViewProtocol {
    func showSpinner() {
        hud.vibrancyEnabled = true
        hud.textLabel.text = Constants.MESSAGES.LOADING
        hud.show(in: self.view)
    }
    
    func dismissSpinner() {
        hud.dismiss(afterDelay: 0)
    }
    
    // TODO: implement view output methods
    func viewAstronomy(with listAstronomyDTO: [AstronomyDTO]) {
        self.listAstronomyDTO = listAstronomyDTO
        self.tblAstronomy.reloadData()
    }
    
    func viewError(with error: String) {
        let alert = UIAlertController(title: Constants.MESSAGES.TITLEALERVIEW, message: error, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: Constants.MESSAGES.BUTTONTITLEACCEPT, style: .default, handler: nil)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
    }
    
}
