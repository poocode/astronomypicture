
import UIKit
import SDWebImage

class RefreshCell: UITableViewCell {

    @IBOutlet weak var oneRight: NSLayoutConstraint!
    @IBOutlet weak var twoRight: NSLayoutConstraint!
    
    @IBOutlet weak var imageUrl: UIImageView!
    @IBOutlet weak var viewTitle: UIView!
    @IBOutlet weak var viewMediaType: UIView!
    @IBOutlet weak var viewDate: UIView!
    
    @IBOutlet weak var stackLegend: UIStackView!
    @IBOutlet weak var txtTitle: UILabel!
    @IBOutlet weak var txtDate: UILabel!
    @IBOutlet weak var txtMediaType: UILabel!
    
    //MARK: Public Methods
    override func layoutSubviews() {
        super.layoutSubviews()
        oneRight.constant = CGFloat(arc4random()%100) + 40
        twoRight.constant = CGFloat(arc4random()%100) + 40
    }
    
    var astronomyDTO: AstronomyDTO! {
           didSet {
               self.fileList()
           }
       }
       
    private func fileList() {
        imageUrl.sd_setImage(with: URL(string: astronomyDTO.url!), placeholderImage: UIImage(named: "placeholderImage"))
        
        viewTitle.isHidden = true
        viewMediaType.isHidden = true
        viewDate.isHidden = true
        
        stackLegend.isHidden = false
        txtTitle.text = astronomyDTO.title
        txtDate.text = astronomyDTO.date
        txtMediaType.text = astronomyDTO.media_type
        
    }
    
    //MARK: Override
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
