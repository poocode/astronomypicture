//
//  HomeRemoteDataManager.swift
//  AstronomyPicture
//
//  Created by German Garcia on 20/08/20.
//  Copyright © 2020 German Garcia. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

class HomeRemoteDataManager: HomeRemoteDataManagerInputProtocol {
    var remoteRequestHandler: HomeRemoteDataManagerOutputProtocol?
    
    func remoteGetAstronomyPictureDay(startDate: String, dateBefore: String) {
        
        Alamofire.request("\(Constants.PARAMETERS.ENDPOINT)planetary/apod?api_key=\(Constants.KEYSERVICE.KEY)&start_date=\(dateBefore)&end_date=\(startDate)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseArray(completionHandler: { (response: DataResponse<[AstronomyDTO]>) in
            switch response.result {
            case .success:
                self.remoteRequestHandler?.callBackResponseAstronomyPictureDay(with: response.result.value!)
            case .failure(let error):
                self.remoteRequestHandler?.callBackResponseError(with: error.localizedDescription)
            }
        })
        
    }
    
}
