//
//  HomeInteractor.swift
//  AstronomyPicture
//
//  Created by German Garcia on 20/08/20.
//  Copyright © 2020 German Garcia. All rights reserved.
//

import Foundation

class HomeInteractor: HomeInteractorInputProtocol {
    
    // MARK: Properties
    weak var presenter: HomeInteractorOutputProtocol?
    var localDatamanager: HomeLocalDataManagerInputProtocol?
    var remoteDatamanager: HomeRemoteDataManagerInputProtocol?
    
    func getAstronomyPictureDay(startDate: String, dateBefore: String) {
        remoteDatamanager?.remoteGetAstronomyPictureDay(startDate: startDate, dateBefore: dateBefore)
    }

}

extension HomeInteractor: HomeRemoteDataManagerOutputProtocol {
    // TODO: Implement use case methods
    
    func callBackResponseAstronomyPictureDay(with listAstronomyDTO: [AstronomyDTO]) {
        presenter?.interatorPushAstronomyPresenter(with: listAstronomyDTO)
    }
    
    func callBackResponseError(with error: String) {
        presenter?.interatorPushError(with: error)
    }
    
}
