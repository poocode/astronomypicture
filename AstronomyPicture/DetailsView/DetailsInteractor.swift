//
//  DetailsInteractor.swift
//  AstronomyPicture
//
//  Created by German Garcia on 20/08/20.
//  Copyright © 2020 German Garcia. All rights reserved.
//

import Foundation

class DetailsInteractor: DetailsInteractorInputProtocol {

    // MARK: Properties
    weak var presenter: DetailsInteractorOutputProtocol?
    var localDatamanager: DetailsLocalDataManagerInputProtocol?
    var remoteDatamanager: DetailsRemoteDataManagerInputProtocol?

}

extension DetailsInteractor: DetailsRemoteDataManagerOutputProtocol {
    // TODO: Implement use case methods
}
