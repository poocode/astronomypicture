//
//  DetailsPresenter.swift
//  AstronomyPicture
//
//  Created by German Garcia on 20/08/20.
//  Copyright © 2020 German Garcia. All rights reserved.
//

import Foundation

class DetailsPresenter  {
    
    // MARK: Properties
    weak var view: DetailsViewProtocol?
    var interactor: DetailsInteractorInputProtocol?
    var wireFrame: DetailsWireFrameProtocol?
    var astronomyDTO: AstronomyDTO?
    
}

extension DetailsPresenter: DetailsPresenterProtocol {
    // TODO: implement presenter methods
    func viewDidLoad() {
        view?.showDetailInfo(with: astronomyDTO!)
    }
}

extension DetailsPresenter: DetailsInteractorOutputProtocol {
    // TODO: implement interactor output methods
}
