//
//  DetailsWireFrame.swift
//  AstronomyPicture
//
//  Created by German Garcia on 20/08/20.
//  Copyright © 2020 German Garcia. All rights reserved.
//

import Foundation
import UIKit

class DetailsWireFrame: DetailsWireFrameProtocol {

    class func createDetailsModule(with astronomyDTO: AstronomyDTO) -> UIViewController {
        
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "detailsView")
        if let view = viewController as? DetailsView {
            let presenter: DetailsPresenterProtocol & DetailsInteractorOutputProtocol = DetailsPresenter()
            let interactor: DetailsInteractorInputProtocol & DetailsRemoteDataManagerOutputProtocol = DetailsInteractor()
            let localDataManager: DetailsLocalDataManagerInputProtocol = DetailsLocalDataManager()
            let remoteDataManager: DetailsRemoteDataManagerInputProtocol = DetailsRemoteDataManager()
            let wireFrame: DetailsWireFrameProtocol = DetailsWireFrame()
            
            view.presenter = presenter
            presenter.view = view
            presenter.wireFrame = wireFrame
            presenter.interactor = interactor
            presenter.astronomyDTO = astronomyDTO
            interactor.presenter = presenter
            interactor.localDatamanager = localDataManager
            interactor.remoteDatamanager = remoteDataManager
            remoteDataManager.remoteRequestHandler = interactor
            
            return viewController
        }
        return UIViewController()
    }
    
    static var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "DetailView", bundle: Bundle.main)
    }
    
}
