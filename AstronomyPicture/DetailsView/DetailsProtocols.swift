//
//  DetailsProtocols.swift
//  AstronomyPicture
//
//  Created by German Garcia on 20/08/20.
//  Copyright © 2020 German Garcia. All rights reserved.
//

import Foundation
import UIKit

protocol DetailsViewProtocol: class {
    // PRESENTER -> VIEW
    var presenter: DetailsPresenterProtocol? { get set }
    func showDetailInfo(with astronomyDTO: AstronomyDTO)
}

protocol DetailsWireFrameProtocol: class {
    // PRESENTER -> WIREFRAME
    static func createDetailsModule(with astronomyDTO: AstronomyDTO) -> UIViewController
}

protocol DetailsPresenterProtocol: class {
    // VIEW -> PRESENTER
    var view: DetailsViewProtocol? { get set }
    var interactor: DetailsInteractorInputProtocol? { get set }
    var wireFrame: DetailsWireFrameProtocol? { get set }
    var astronomyDTO: AstronomyDTO? { get set }
    
    func viewDidLoad()
}

protocol DetailsInteractorOutputProtocol: class {
// INTERACTOR -> PRESENTER
}

protocol DetailsInteractorInputProtocol: class {
    // PRESENTER -> INTERACTOR
    var presenter: DetailsInteractorOutputProtocol? { get set }
    var localDatamanager: DetailsLocalDataManagerInputProtocol? { get set }
    var remoteDatamanager: DetailsRemoteDataManagerInputProtocol? { get set }
}

protocol DetailsDataManagerInputProtocol: class {
    // INTERACTOR -> DATAMANAGER
}

protocol DetailsRemoteDataManagerInputProtocol: class {
    // INTERACTOR -> REMOTEDATAMANAGER
    var remoteRequestHandler: DetailsRemoteDataManagerOutputProtocol? { get set }
}

protocol DetailsRemoteDataManagerOutputProtocol: class {
    // REMOTEDATAMANAGER -> INTERACTOR
}

protocol DetailsLocalDataManagerInputProtocol: class {
    // INTERACTOR -> LOCALDATAMANAGER
}
