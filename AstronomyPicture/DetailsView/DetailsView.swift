//
//  DetailsView.swift
//  AstronomyPicture
//
//  Created by German Garcia on 20/08/20.
//  Copyright © 2020 German Garcia. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
import XCDYouTubeKit
import AVKit

class DetailsView: UIViewController {

    @IBOutlet weak var imgDetailInfo: UIImageView!
    @IBOutlet weak var txtTitle: UILabel!
    @IBOutlet weak var txtDate: UILabel!
    @IBOutlet weak var txtVesion: UILabel!
    @IBOutlet weak var txtMediaType: UILabel!
    @IBOutlet weak var txtExplanation: UILabel!
    
    // MARK: Properties
    var presenter: DetailsPresenterProtocol?
    var astronomyDTOView = AstronomyDTO()
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imgDetailInfo.isUserInteractionEnabled = true
        imgDetailInfo.addGestureRecognizer(tapGestureRecognizer)
        
        presenter?.viewDidLoad()
        
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        if astronomyDTOView.media_type!.elementsEqual("video") {
            let playerViewController = AVPlayerViewController()
            self.present(playerViewController, animated: true, completion: nil)
            if let index = astronomyDTOView.url!.range(of: "embed/")?.upperBound {
                let strIdentifier = String(astronomyDTOView.url![index...])
                XCDYouTubeClient.default().getVideoWithIdentifier(strIdentifier) { (video: XCDYouTubeVideo?, error: Error?) in
                    if let streamURL = video?.streamURLs[XCDYouTubeVideoQuality.HD720.rawValue] {
                        playerViewController.player = AVPlayer(url: streamURL)
                        playerViewController.player!.play()
                    } else {
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            }
        }
    }
}

extension DetailsView: DetailsViewProtocol {
    // TODO: implement view output methods
    func showDetailInfo(with astronomyDTO: AstronomyDTO) {
        self.astronomyDTOView = astronomyDTO
        self.title = astronomyDTO.title
        txtTitle.text = astronomyDTO.title
        txtDate.text = astronomyDTO.date
        txtMediaType.text = astronomyDTO.media_type
        txtVesion.text = astronomyDTO.service_version
        txtExplanation.text = astronomyDTO.explanation
        
        if (astronomyDTO.media_type!.elementsEqual("image")) {
            imgDetailInfo.sd_setImage(with: URL(string: astronomyDTO.hdurl!), placeholderImage: UIImage(named: "placeholderImage"))
        } else {
            imgDetailInfo.image = UIImage(named: "playVideo")!
        }
        
    }
}
